/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

//#ifndef __PLUGINEDITOR_H_8FBA87C0__
//#define __PLUGINEDITOR_H_8FBA87C0__
#pragma once
#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "GuiLookAndFeel.h"

class ModalCallback;

//==============================================================================
/**
*/
class Paj_maagEq4AudioProcessorEditor  : public AudioProcessorEditor,
	                                     public Slider::Listener,
									     public ComboBox::Listener,
									     public Timer,
									     public Button::Listener,
									     public TooltipClient
{
public:
    GuiLookAndFeel guiLookAndFeel;
	Paj_maagEq4AudioProcessorEditor (Paj_maagEq4AudioProcessor* ownerFilter, Paj_maagEq4AudioProcessor::GUIType type);
	~Paj_maagEq4AudioProcessorEditor();

    
  //==============================================================================
  // This is just a standard Juce paint method...
	void resized();
	void paint (Graphics& g);

	void timerCallback();

	void sliderValueChanged (Slider* slider);
	void comboBoxChanged (ComboBox* comboBoxThatHasChanged);
	void buttonClicked (Button* button);

	void mouseDown(const MouseEvent& e);

	String getTooltip();
	void modalReturn(int returnValue);

private:


	class ModalCallback : public ModalComponentManager::Callback
	{
	public:
		ModalCallback(Paj_maagEq4AudioProcessorEditor* parent_)
			: parent(parent_)
		{
		}

		void modalStateFinished(int returnValue)
		{
			if (parent != nullptr)
				parent->modalReturn(returnValue);
		}
	private:
		Paj_maagEq4AudioProcessorEditor* parent;
	};

	void initGui();
	void resizedGui();
	void paintGui(Graphics& g);
	void updateSlidersGui();
	void timerCallbackGui();

	void updateTooltipState();

	Paj_maagEq4AudioProcessor* Proc;

	const Paj_maagEq4AudioProcessor::GUIType guiType;

	GuiSlider guiSliders[EqDsp::kNumTypes];

	Label labels[EqDsp::kNumTypes];
	ComboBox type;

	ToggleButton mastering;
	ToggleButton analog;
	ToggleButton keepGain;

	ToggleButton types[EqDsp::kNumHighSelves];

	GuiSlider guiMasterVol;

	Label masterVolLabel;

	Image background;

	ScopedPointer<TooltipWindow> tooltips;

	ModalCallback* modalCallback;
	ScopedPointer<PopupMenu> showTooltips;


    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(Paj_maagEq4AudioProcessorEditor)
};



//#endif  // __PLUGINEDITOR_H_8FBA87C0__
